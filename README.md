# About

Это один из репозиториев моего проекта по dev-ops (Созданию процесса непрерывной поставки с обратной связью). Тут описан процесс деплоя приложения с помощью helm3

## How to start

1. Install helm 3
2. Create k8s manifests
3. Create tamplates from manifests